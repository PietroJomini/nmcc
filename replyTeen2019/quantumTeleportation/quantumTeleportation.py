
import math


def dist(x1, y1, x2, y2):
    return math.sqrt((x1 - x2)**2 + (y1-y2)**2)


def step(x1, y1, x2, y2):
    return abs(x1-x2) + abs(y1-y2)


def nearest(x, y, portals):
    n_nindex = -1
    m_dist = -1
    k = 0
    for p in portals:
        pdist = step(x, y, p[0], p[1])
        if pdist < m_dist or m_dist == -1:
            m_dist = pdist
            n_nindex = k
        k += 1

    p = portals[n_nindex]
    del portals[n_nindex]

    return p


def case(x, y, portals):
    DIV = 100003
    stot = 0
    while len(portals) > 0:
        p = nearest(x, y, portals)
        nstep = step(x, y, p[0], p[1])
        x, y = (p[2], p[3])
        stot = ((stot % DIV) + (nstep % DIV)) % DIV

    return stot


with open('input.txt') as i:
    lines = i.read().split('\n')

pos = 1
T = int(lines[0])
cases = []

for i in range(T):

    dim = tuple(int(n) for n in lines[pos].split(' '))
    x, y = (int(n) for n in lines[pos+1].split(' '))
    n = int(lines[pos+2])
    portals = [tuple(int(n) for n in line.split(' '))
               for line in lines[pos+3:pos+3+n]]
    pos += 3 + n

    cases.append(case(x, y, sorted(portals)))

with open('output.txt', 'w') as o:
    k = 0
    for c in cases:
        k += 1
        o.write(f'Case #{k}: {c}' + ('\n' if len(cases) != k else ''))
