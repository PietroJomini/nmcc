from functools import cmp_to_key
from itertools import product

t = [
    (),                     # 0
    (),                     # 1
    ('A', 'B', 'C'),        # 2
    ('D', 'E', 'F'),        # 3
    ('G', 'H', 'I'),        # 4
    ('J', 'K', 'L'),        # 5
    ('M', 'N', 'O'),        # 6
    ('P', 'Q', 'R', 'S'),   # 7
    ('T', 'U', 'V'),        # 8
    ('W', 'X', 'Y', 'Z')    # 9
]

alp = list('abcdefghijklmnopqrstuvwxyz'.upper())


def mixx(keys):
    if len(keys) == 1:
        return keys[0]
    else:
        res = []
        subk = mixx(keys[1:])
        for s in subk:
            for k in keys[0]:
                res.append(f'{k}{s}')
        return res


def score(seq, table):
    s = 0
    for i in range(len(seq) - 1):
        s += table[alp.index(seq[i])][alp.index(seq[i+1])]
    return s


def sort(a, b):
    if a[0] != b[0]:
        return a[0] - b[0]
    else:
        return alp.index(b[1][0]) - alp.index(a[1][0])


def case(keys, table, k):
    # mixed = mixx(keys)
    mixed = [''.join(p) for p in product(*keys)]
    print(mixed)
    scored = []
    for m in mixed:
        s = score(m, table)
        scored.append((s, m))
    return list(reversed(sorted(scored, key=cmp_to_key(sort))))[k-1][1]


with open('input.txt') as i:
    lines = i.read().split('\n')

pos = 1
T = int(lines[0])
cases = []

for i in range(T):
    L, K = (int(k) for k in lines[pos].split(' '))
    table = [[int(k) for k in line.split(' ')]
             for line in lines[pos+1:pos + 27]]
    pos += 26
    keys = [t[int(k)] for k in lines[pos+1]]
    pos += 2
    cases.append(case(keys, table, K))

with open('output.txt', 'w') as o:
    k = 0
    for c in cases:
        k += 1
        o.write(f'Case #{k}: {c}' + ('\n' if len(cases) != k else ''))
